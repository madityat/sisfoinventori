<!DOCTYPE hmtl>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login - Sistem Informasi Pengelola Inventori</title>

    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div id="wrapper">
        <div class="container">
            <div align="center" style="margin-top:210px;">
                <form action="" method="post" class="navbar-form">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input type="text" name= "user" class="form-control" placeholder="Username" required autofocus/>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input type="password" name= "pass" class="form-control" placeholder="Password" required/>
                    </div>
                    <div class="input-group">
                        <input type="submit" name="login" class="btn btn-primary" value="Login">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>