<?php
    session_start();
    if (!isset($_SESSION['login'])) {
        header("Location: ../auth");
        die();
    }

    require_once('../config/koneksi.php');
    require_once('../models/database.php');

    $connection = new Database($host, $user, $pass, $database);
    include "../models/m_pengajuan.php";

    $pgj = new Pengajuan($connection);
    $tampil = $pgj->tampil($_GET['id']);
    
    function getBulan($bulan) {
        $bulanArray = array(
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember',
        );
        return $bulanArray[$bulan];
    }
    $template = file_get_contents('template_surat.html');
    if ($tampil) {
        $data = $tampil->fetch_object();
        $origDate = $data->createdAt; 
        $tanggal = date("d m Y", strtotime($origDate));
        $tanggal = str_replace(date("m", strtotime($origDate)) ,getBulan(date("m", strtotime($origDate))) ,$tanggal);

        $template = str_replace('[lokasi]', 'Bandung', $template);
        $template = str_replace('[tanggal]', $tanggal, $template);
        $template = str_replace('[nama_pengaju]', $data->nama_pengaju, $template);
        $template = str_replace('[nip]', $data->nip, $template);
        $template = str_replace('[divisi_kerja]', $data->divisi_kerja, $template);
        $template = str_replace('[kd_barang]', $data->kd_barang, $template);
        $template = str_replace('[nama_barang]', $data->nama_barang, $template);
        $template = str_replace('[jenis_barang]', $data->jenis_barang, $template);
        $template = str_replace('[keterangan]', $data->keterangan, $template);
    }
?>
<div id="surat_pengajuan" style="border: 1px solid black;width: auto;margin: auto;padding:1%;">
    <?php echo $template; ?>
</div>
