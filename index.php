<?php
  session_start();
  if (!isset($_SESSION['login'])) {
    header("Location: ./auth");
    die();
  }

  require_once('config/koneksi.php');
  require_once('models/database.php');

  $connection = new Database($host, $user, $pass, $database);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Peminjaman Barang</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="assets/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="">Pengelola Inventori TVRI</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <?php
            include 'menu_'.$_SESSION['login_as'].'.php';
          ?>
          <ul class="nav navbar-nav navbar-right navbar-user">          
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo ucfirst($_SESSION['login_as']); ?> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="./auth/logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>

      <div id="page-wrapper">

        <?php
        if(@$_GET['page'] == 'dashboard' || @$_GET['page'] == ''){
          include "views/dashboard.php";
        } else if (@$_GET['page'] == 'barang_input'){
          include "views/barang_input.php";
        } else if (@$_GET['page'] == 'barang_edit'){
          include "views/barang_edit.php";
        } else if (@$_GET['page'] == 'barang_data'){
          include "views/barang_data.php";
        } else if (@$_GET['page'] == 'barang_dipinjam_data'){
          include "views/barang_dipinjam_data.php";
        } else if (@$_GET['page'] == 'peminjaman_data'){
          include "views/peminjaman_data.php";
        } else if (@$_GET['page'] == 'peminjaman_input'){
          include "views/peminjaman_input.php";
        } else if (@$_GET['page'] == 'peminjaman_riwayat'){
          include "views/peminjaman_riwayat.php";
        }else if (@$_GET['page'] == 'pengembalian_input'){
          include "views/pengembalian_input.php";
        }else if (@$_GET['page'] == 'dashboard_pegawai'){
          include "views/dashboard_pegawai.php";
        }else if (@$_GET['page'] == 'pengajuan_input'){
          include "views/pengajuan_input.php";
        }else if (@$_GET['page'] == 'pengajuan_data'){
          include "views/pengajuan_data.php";
        }
        ?>

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <script src="assets/js/bootstrap.js"></script>

  </body>
</html>